package test.silverbars.services;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import test.silverbars.domain.PriceSummary;

import java.math.BigDecimal;
import java.util.List;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class PriceSummaryServiceTest {
    private final TreeMap<BigDecimal, BigDecimal> aggregatedView = new TreeMap<>();
    private PriceSummaryService underTest = new PriceSummaryService(aggregatedView);

    @Before
    public void setUp() throws Exception {
        aggregatedView.clear();
    }

    @Test
    public void accumulateNew() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);

        underTest.accumulate(price, quantity);

        assertThat(aggregatedView.containsKey(new BigDecimal(1)), Matchers.is(true));
        assertThat(aggregatedView.get(new BigDecimal(1)), Matchers.is(quantity));
    }

    @Test
    public void accumulateExisting() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);

        aggregatedView.put(price, new BigDecimal(4));

        underTest.accumulate(price, quantity);

        assertThat(aggregatedView.containsKey(new BigDecimal(1)), Matchers.is(true));
        assertThat(aggregatedView.get(new BigDecimal(1)), Matchers.is(new BigDecimal(6)));
    }

    @Test(expected = IllegalStateException.class) // Library invariant violated!
    public void removeUnknown() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);

        underTest.remove(price, quantity);
    }

    @Test
    public void removeWithRemainder() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);

        aggregatedView.put(price, new BigDecimal(4));

        underTest.remove(price, quantity);

        assertThat(aggregatedView.containsKey(new BigDecimal(1)), Matchers.is(true));
        assertThat(aggregatedView.get(new BigDecimal(1)), Matchers.is(new BigDecimal(2)));
    }

    @Test
    public void removeWithoutRemainder() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);

        aggregatedView.put(price, new BigDecimal(2));

        underTest.remove(price, quantity);

        assertThat(aggregatedView.containsKey(new BigDecimal(1)), Matchers.is(false));
    }

    @Test
    public void priceSummaryLessThan3() {
        List<PriceSummary> result = underTest.priceSummary();

        assertThat(result, Matchers.hasSize(0));
    }

    @Test
    public void priceSummaryExactly3() {
        aggregatedView.put(new BigDecimal(2), new BigDecimal(2));
        aggregatedView.put(new BigDecimal(1), new BigDecimal(2));
        aggregatedView.put(new BigDecimal(3), new BigDecimal(2));

        List<PriceSummary> result = underTest.priceSummary();

        assertThat(result, Matchers.hasSize(3));
        assertThat(result.get(0).getPrice(), Matchers.is(new BigDecimal(1)));
        assertThat(result.get(1).getPrice(), Matchers.is(new BigDecimal(2)));
        assertThat(result.get(2).getPrice(), Matchers.is(new BigDecimal(3)));
    }

    @Test
    public void priceSummaryMoreThan3() {
        aggregatedView.put(new BigDecimal(2), new BigDecimal(2));
        aggregatedView.put(new BigDecimal(1), new BigDecimal(2));
        aggregatedView.put(new BigDecimal(4), new BigDecimal(2));
        aggregatedView.put(new BigDecimal(3), new BigDecimal(2));

        List<PriceSummary> result = underTest.priceSummary();

        assertThat(result, Matchers.hasSize(3));
        assertThat(result.get(0).getPrice(), Matchers.is(new BigDecimal(1)));
        assertThat(result.get(1).getPrice(), Matchers.is(new BigDecimal(2)));
        assertThat(result.get(2).getPrice(), Matchers.is(new BigDecimal(3)));
    }
}