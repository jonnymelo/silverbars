package test.silverbars.services;

import org.junit.Test;
import test.silverbars.domain.LiveBoard;
import test.silverbars.domain.PriceSummary;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LiveBoardServiceTest {
    private final PriceSummaryService buySummary = mock(PriceSummaryService.class);
    private final PriceSummaryService sellSummary = mock(PriceSummaryService.class);
    private LiveBoardService underTest = new LiveBoardService(buySummary, sellSummary);

    @Test
    public void accumulateSell() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);
        boolean isSellOrder = true;

        underTest.accumulate(price, quantity, isSellOrder);

        verify(sellSummary).accumulate(price, quantity);
        verify(buySummary, never()).accumulate(any(), any());
    }

    @Test
    public void accumulateBuy() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);
        boolean isSellOrder = false;

        underTest.accumulate(price, quantity, isSellOrder);

        verify(buySummary).accumulate(price, quantity);
        verify(sellSummary, never()).accumulate(any(), any());
    }

    @Test
    public void removeSell() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);
        boolean isSellOrder = true;

        underTest.remove(price, quantity, isSellOrder);

        verify(sellSummary).remove(price, quantity);
        verify(buySummary, never()).remove(any(), any());
    }

    @Test
    public void removeBuy() {
        BigDecimal price = new BigDecimal(1);
        BigDecimal quantity = new BigDecimal(2);
        boolean isSellOrder = false;

        underTest.remove(price, quantity, isSellOrder);

        verify(buySummary).remove(price, quantity);
        verify(sellSummary, never()).remove(any(), any());
    }

    @Test
    public void liveBoard() {
        List<PriceSummary> buySummaryList = singletonList(mock(PriceSummary.class));
        List<PriceSummary> sellSummaryList = singletonList(mock(PriceSummary.class));

        when(buySummary.priceSummary()).thenReturn(buySummaryList);
        when(sellSummary.priceSummary()).thenReturn(sellSummaryList);

        LiveBoard result = underTest.liveBoard();

        assertSame(buySummaryList, result.getBuy());
        assertSame(sellSummaryList, result.getSell());
    }
}