package test.silverbars.services;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import test.silverbars.domain.Order;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class OrderRepositoryTest {
    private final HashMap<String, Order> data = new HashMap<>();
    private OrderRepository underTest = new OrderRepository(data);

    @Before
    public void setUp() throws Exception {
        data.clear();
    }

    @Test
    public void insert() {
        String userId = "userId";
        BigDecimal quantity = new BigDecimal(1);
        BigDecimal price = new BigDecimal(2);
        boolean isSellOrder = true;

        Order result = underTest.insert(userId, quantity, price, isSellOrder);

        assertThat(result.getId(), Matchers.notNullValue());
        assertThat(result.getPrice(), Matchers.is(price));
        assertThat(result.getQuantity(), Matchers.is(quantity));
        assertThat(result.isSellOrder(), Matchers.is(isSellOrder));

        assertSame(result, data.get(result.getId()));
    }

    @Test
    public void removeNotPresent() {
        Optional<Order> result = underTest.delete("randomId");

        assertThat(result.isPresent(), Matchers.is(false));
    }

    @Test
    public void removePresent() {
        String orderId = "randomId";

        Order order = mock(Order.class);

        data.put(orderId, order);

        Optional<Order> result = underTest.delete(orderId);

        assertThat(result.isPresent(), Matchers.is(true));
        assertSame(order, result.get());
        assertThat(data.containsKey(orderId), Matchers.is(false));
    }
}