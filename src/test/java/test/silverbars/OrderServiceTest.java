package test.silverbars;

import org.hamcrest.Matchers;
import org.junit.Test;
import test.silverbars.domain.LiveBoard;
import test.silverbars.domain.Order;
import test.silverbars.services.LiveBoardService;
import test.silverbars.services.OrderRepository;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    private final OrderRepository orderRepository = mock(OrderRepository.class);
    private final LiveBoardService liveBoardService = mock(LiveBoardService.class);
    private final OrderService underTest = new OrderService(orderRepository, liveBoardService);

    @Test
    public void register() {
        String userId = "user1";
        BigDecimal price = new BigDecimal(2);
        BigDecimal quantity = new BigDecimal(1);
        boolean isSellOrder = true;

        Order order = mock(Order.class);

        when(orderRepository.insert(userId, quantity, price, isSellOrder)).thenReturn(order);

        Order result = underTest.register(userId, quantity, price, isSellOrder);

        verify(liveBoardService).accumulate(price, quantity, isSellOrder);

        assertSame(order, result);
    }

    @Test
    public void cancelNothingPresent() {
        String orderId = "randomId";

        when(orderRepository.delete(orderId)).thenReturn(Optional.empty());

        boolean result = underTest.cancel(orderId);

        assertThat(result, Matchers.is(false));
    }

    @Test
    public void cancel() {
        String orderId = "randomId";
        BigDecimal price = new BigDecimal(2);
        BigDecimal quantity = new BigDecimal(1);
        boolean isSellOrder = true;

        Order order = mock(Order.class);

        when(orderRepository.delete(orderId)).thenReturn(Optional.of(order));
        when(order.getPrice()).thenReturn(price);
        when(order.getQuantity()).thenReturn(quantity);
        when(order.isSellOrder()).thenReturn(isSellOrder);

        boolean result = underTest.cancel(orderId);

        verify(liveBoardService).remove(price, quantity, isSellOrder);

        assertThat(result, Matchers.is(true));
    }

    @Test
    public void liveBoardJustDelegatesToLiveBoardService() {
        LiveBoard liveBoard = mock(LiveBoard.class);

        when(liveBoardService.liveBoard()).thenReturn(liveBoard);

        LiveBoard result = underTest.liveBoard();

        assertSame(liveBoard, result);
    }
}