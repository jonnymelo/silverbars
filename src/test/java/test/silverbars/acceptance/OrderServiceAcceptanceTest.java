package test.silverbars.acceptance;


import org.hamcrest.Matchers;
import org.junit.Test;
import test.silverbars.OrderService;
import test.silverbars.domain.LiveBoard;
import test.silverbars.domain.Order;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;

public class OrderServiceAcceptanceTest {
    private final OrderService orderService = OrderService.orderService();

    @Test
    public void registerSellOrdersAndSellSummary() {
        orderService.register("user1", new BigDecimal(3.5), new BigDecimal(306), true);
        orderService.register("user2", new BigDecimal(1.2), new BigDecimal(310), true);
        orderService.register("user3", new BigDecimal(1.5), new BigDecimal(307), true);
        orderService.register("user4", new BigDecimal(2.0), new BigDecimal(306), true);

        LiveBoard liveBoard = orderService.liveBoard();

        assertThat(liveBoard.getSell(), Matchers.hasSize(3));
        // 1st
        assertThat(liveBoard.getSell().get(0).getQuantity(), Matchers.is(new BigDecimal(5.5)));
        assertThat(liveBoard.getSell().get(0).getPrice(), Matchers.is(new BigDecimal(306)));
        // 2nd
        assertThat(liveBoard.getSell().get(1).getQuantity(), Matchers.is(new BigDecimal(1.5)));
        assertThat(liveBoard.getSell().get(1).getPrice(), Matchers.is(new BigDecimal(307)));
        // 3rd
        assertThat(liveBoard.getSell().get(2).getQuantity(), Matchers.is(new BigDecimal(1.2)));
        assertThat(liveBoard.getSell().get(2).getPrice(), Matchers.is(new BigDecimal(310)));
    }

    @Test
    public void cancelAndSummary() {
        Order order = orderService.register("user1", new BigDecimal(3.5), new BigDecimal(306), true);

        boolean result = orderService.cancel(order.getId());
        LiveBoard liveBoard = orderService.liveBoard();

        assertThat(result, Matchers.is(true));
        assertThat(liveBoard.getSell(), Matchers.hasSize(0));
    }

    @Test
    public void registerBuyOrdersAndBuySummary() {
        orderService.register("user1", new BigDecimal(3.5), new BigDecimal(306), false);
        orderService.register("user2", new BigDecimal(1.2), new BigDecimal(310), false);
        orderService.register("user3", new BigDecimal(1.5), new BigDecimal(307), false);
        orderService.register("user4", new BigDecimal(2.0), new BigDecimal(306), false);

        LiveBoard liveBoard = orderService.liveBoard();

        assertThat(liveBoard.getBuy(), Matchers.hasSize(3));
        // 1st
        assertThat(liveBoard.getBuy().get(0).getQuantity(), Matchers.is(new BigDecimal(1.2)));
        assertThat(liveBoard.getBuy().get(0).getPrice(), Matchers.is(new BigDecimal(310)));
        // 2nd
        assertThat(liveBoard.getBuy().get(1).getQuantity(), Matchers.is(new BigDecimal(1.5)));
        assertThat(liveBoard.getBuy().get(1).getPrice(), Matchers.is(new BigDecimal(307)));
        // 3rd
        assertThat(liveBoard.getBuy().get(2).getQuantity(), Matchers.is(new BigDecimal(5.5)));
        assertThat(liveBoard.getBuy().get(2).getPrice(), Matchers.is(new BigDecimal(306)));
    }

    @Test
    public void liveBoardWithMaximumOf3BuyItems() {
        orderService.register("user1", new BigDecimal(3.5), new BigDecimal(306), false);
        orderService.register("user2", new BigDecimal(1.2), new BigDecimal(310), false);
        orderService.register("user3", new BigDecimal(1.5), new BigDecimal(307), false);
        orderService.register("user4", new BigDecimal(2.0), new BigDecimal(306), false);
        orderService.register("user4", new BigDecimal(2.0), new BigDecimal(301), false);

        LiveBoard liveBoard = orderService.liveBoard();

        assertThat(liveBoard.getBuy(), Matchers.hasSize(3));
        // 1st
        assertThat(liveBoard.getBuy().get(0).getQuantity(), Matchers.is(new BigDecimal(1.2)));
        assertThat(liveBoard.getBuy().get(0).getPrice(), Matchers.is(new BigDecimal(310)));
        // 2nd
        assertThat(liveBoard.getBuy().get(1).getQuantity(), Matchers.is(new BigDecimal(1.5)));
        assertThat(liveBoard.getBuy().get(1).getPrice(), Matchers.is(new BigDecimal(307)));
        // 3rd
        assertThat(liveBoard.getBuy().get(2).getQuantity(), Matchers.is(new BigDecimal(5.5)));
        assertThat(liveBoard.getBuy().get(2).getPrice(), Matchers.is(new BigDecimal(306)));
    }


    @Test
    public void liveBoardWithMaximumOf3SellItems() {
        orderService.register("user1", new BigDecimal(3.5), new BigDecimal(306), true);
        orderService.register("user2", new BigDecimal(1.2), new BigDecimal(310), true);
        orderService.register("user3", new BigDecimal(1.5), new BigDecimal(307), true);
        orderService.register("user4", new BigDecimal(2.0), new BigDecimal(306), true);
        orderService.register("user5", new BigDecimal(2.0), new BigDecimal(316), true);

        LiveBoard liveBoard = orderService.liveBoard();

        assertThat(liveBoard.getSell(), Matchers.hasSize(3));
        // 1st
        assertThat(liveBoard.getSell().get(0).getQuantity(), Matchers.is(new BigDecimal(5.5)));
        assertThat(liveBoard.getSell().get(0).getPrice(), Matchers.is(new BigDecimal(306)));
        // 2nd
        assertThat(liveBoard.getSell().get(1).getQuantity(), Matchers.is(new BigDecimal(1.5)));
        assertThat(liveBoard.getSell().get(1).getPrice(), Matchers.is(new BigDecimal(307)));
        // 3rd
        assertThat(liveBoard.getSell().get(2).getQuantity(), Matchers.is(new BigDecimal(1.2)));
        assertThat(liveBoard.getSell().get(2).getPrice(), Matchers.is(new BigDecimal(310)));
    }
}
