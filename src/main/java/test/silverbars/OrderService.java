package test.silverbars;

import test.silverbars.domain.LiveBoard;
import test.silverbars.domain.Order;
import test.silverbars.services.LiveBoardService;
import test.silverbars.services.OrderRepository;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Memory: O(n) - Number of active orders
 */
public class OrderService {
    public static OrderService orderService () {
        return new OrderService(
                new OrderRepository(),
                new LiveBoardService()
        );
    }

    private final OrderRepository orderRepository;
    private final LiveBoardService liveBoardService;

    public OrderService(OrderRepository orderRepository, LiveBoardService liveBoardService) {
        this.orderRepository = orderRepository;
        this.liveBoardService = liveBoardService;
    }

    /**
     * Execution Time: O(log(n))
     */
    public Order register (String userId, BigDecimal quantity, BigDecimal price, boolean isSellOrder) {
        Order newOrder = orderRepository.insert(userId, quantity, price, isSellOrder);
        liveBoardService.accumulate(price, quantity, isSellOrder);
        return newOrder;
    }


    /**
     * Execution Time: O(log(n))
     */
    public boolean cancel(String id) {
        Optional<Order> potentialOrder = orderRepository.delete(id);

        if (potentialOrder.isPresent()) {
            Order order = potentialOrder.get();
            liveBoardService.remove(order.getPrice(), order.getQuantity(), order.isSellOrder());
            return true;
        }

        return false;
    }

    /**
     * Execution Time: O(k)
     */
    public LiveBoard liveBoard() {
        return liveBoardService.liveBoard();
    }
}
