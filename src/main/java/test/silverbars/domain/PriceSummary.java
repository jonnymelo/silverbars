package test.silverbars.domain;

import java.math.BigDecimal;

public class PriceSummary {
    private final BigDecimal price;
    private final BigDecimal quantity;

    public PriceSummary(BigDecimal price, BigDecimal quantity) {
        this.quantity = quantity;
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
