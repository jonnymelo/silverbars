package test.silverbars.domain;

public enum OrderType {
    BUY, SELL
}
