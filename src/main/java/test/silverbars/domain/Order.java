package test.silverbars.domain;

import java.math.BigDecimal;

public class Order {
    private final String id;
    private final String userId;
    private final BigDecimal quantity;
    private final BigDecimal price;
    private final boolean sellOrder;

    public Order(String id, String userId, BigDecimal quantity, BigDecimal price, boolean sellOrder) {
        this.id = id;
        this.userId = userId;
        this.quantity = quantity;
        this.price = price;
        this.sellOrder = sellOrder;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public boolean isSellOrder() {
        return sellOrder;
    }
}
