package test.silverbars.domain;

import java.util.List;

public class LiveBoard {
    private final List<PriceSummary> sell;
    private final List<PriceSummary> buy;

    public LiveBoard(List<PriceSummary> sell, List<PriceSummary> buy) {
        this.sell = sell;
        this.buy = buy;
    }

    public List<PriceSummary> getSell() {
        return sell;
    }

    public List<PriceSummary> getBuy() {
        return buy;
    }
}
