package test.silverbars.services;

import test.silverbars.domain.LiveBoard;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.function.Function;

public class LiveBoardService {
    private final PriceSummaryService buySummary;
    private final PriceSummaryService sellSummary;

    LiveBoardService(PriceSummaryService buySummary, PriceSummaryService sellSummary) {
        this.buySummary = buySummary;
        this.sellSummary = sellSummary;
    }

    public LiveBoardService() {
        this(new PriceSummaryService(Comparator.<BigDecimal, BigDecimal>comparing(Function.identity()).reversed()), new PriceSummaryService(BigDecimal::compareTo));
    }

    public void accumulate(BigDecimal price, BigDecimal quantity, boolean isSellOrder) {
        if (isSellOrder) {
            sellSummary.accumulate(price, quantity);
        } else {
            buySummary.accumulate(price, quantity);
        }
    }

    public void remove(BigDecimal price, BigDecimal quantity, boolean isSellOrder) {
        if (isSellOrder) {
            sellSummary.remove(price, quantity);
        } else {
            buySummary.remove(price, quantity);
        }
    }

    public LiveBoard liveBoard() {
        return new LiveBoard(
                sellSummary.priceSummary(),
                buySummary.priceSummary()
        );
    }
}
