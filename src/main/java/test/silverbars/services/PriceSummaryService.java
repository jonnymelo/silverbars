package test.silverbars.services;

import test.silverbars.domain.PriceSummary;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class PriceSummaryService {
    private final SortedMap<BigDecimal, BigDecimal> aggregatedView;

    public PriceSummaryService(Comparator<BigDecimal> ordering) {
        aggregatedView = Collections.synchronizedSortedMap(new TreeMap<>(ordering));
    }

    PriceSummaryService(SortedMap<BigDecimal, BigDecimal> aggregatedView) {
        this.aggregatedView = aggregatedView;
    }

    public void accumulate(BigDecimal price, BigDecimal quantity) {
        aggregatedView.compute(price, (key, oldValue) -> {
            if (oldValue == null) {
                return quantity;
            } else {
                return quantity.add(oldValue);
            }
        });
    }

    public void remove(BigDecimal price, BigDecimal quantity) {
        aggregatedView.compute(price, (key, oldValue) -> {
            if (oldValue == null)
                throw new IllegalStateException(String.format(
                        "Invariant violated! No quantity associated with price %s",
                        key
                ));

            BigDecimal newValue = oldValue.subtract(quantity);

            if (newValue.compareTo(BigDecimal.ZERO) == 0) return null;
            else return newValue;
        });
    }

    public List<PriceSummary> priceSummary() {
        return aggregatedView.entrySet().stream()
                        .limit(3)
                        .map(x -> new PriceSummary(x.getKey(), x.getValue()))
                        .collect(Collectors.toList());
    }
}
