package test.silverbars.services;

import test.silverbars.domain.Order;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class OrderRepository {
    private final Map<String, Order> data;

    OrderRepository(Map<String, Order> data) {
        this.data = data;
    }

    public OrderRepository() {
        this(new ConcurrentHashMap<>()); // Concurrent map to ensure this library supports parallelism
    }

    public Order insert(String userId, BigDecimal quantity, BigDecimal price, boolean isSellOrder) {
        String id = UUID.randomUUID().toString();

        Order newOrder = new Order(
                id,
                userId, quantity,
                price,
                isSellOrder
        );

        data.compute(newOrder.getId(), (key, order) -> {
            if (order != null) throw new IllegalStateException(String.format("Order with id %s already exists", key));
            // I'm throwing an exception here rather than using a functional approach because this is a completely unexpected scenario,
            // I'm obviously making an assumption that UUID collision would be a vert edge case scenario here. If the volume or orders
            // got any near to the size of the UUID domain, then we would have to change the strategy.

            return newOrder;
        });

        return newOrder;
    }

    public Optional<Order> delete(String id) {
        return Optional.ofNullable(data.remove(id));
    }
}
