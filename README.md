# Silver Bars
    
## Tech Stack

- Java JDK 11 - Used this version because version 8 is end of life, also 11 is more performant memory wise.
- Gradle 5 - Enough for this exercise (added in wrapped mode)
- Junit/Mockito/Hamcrest triplet (dependencies in build.gradle)
    
## Run tests

    ./gradlew test
    
## Example Code

Check OrderServiceAcceptanceTest::registerSellOrdersAndSellSummary

## Assumptions on Non-functional Requirements

- The library needs to be capable to handle concurrency
- Performance wise, in worst case scenario, all operations should execute in O(log n) on the number of active orders 
- Still performance wise, worst case scenario the memory should be within O(n) on the number of active orders 
- The produced artifact is a java library (jar) which can be used by clients
(I'm assuming the UI team here has this capability)
- Eventual consistency was used in the live board, so, I deemed ok to have sudden discrepancies in the live board compared to the real number of active orders in the system. 
This also allowed me to improve the performance of computing the live board, which takes O(k)  


## Assumptions on Functional Requirements

- The register will return the registered order with the generated id for it.
- The cancel feature will return a boolean, which tells the user if the order was found (true) or not (false)
- To use the cancel feature, an order needs to registered first. 
- The live board returns two lists, the top 3 of the aggregated view of both BUY and SELL sides.

# Assumptions on infrastructure

- Memory would not be an issue, application is not prepared to recover from Out of Memory.

## Development Method

1. Used ATDD starting with the acceptance test OrderServiceAcceptanceTest::registerSellOrdersAndSellSummary
which allowed me to define the basic API
2. Implemented an MVP meeting the acceptance criteria
3. Refactored the code applying IoC concepts
4. Used TDD at unit level to guarantee the contract between the distinct collaborators

## Notes

- I don't tend to write comments, only left comments explaining why some specific decisions were made also documenting the cost of the main operations/storage
- Used plain old dependency injection (via constructor) as I tend to prefer code traceability/readability over dependencies scattered along the code base using reflection magic to be injected.
- I would put this library under a load test using, probably, gatling if the exercise asked for proper non-functional requirements.